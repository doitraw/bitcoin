###### BitcoinApp

### Description

Simple project made in Kotlin, using Rx, Koin, MVP.

### What's left to do

- Unit tests!
- DiffUtils(had it initially, but for some reason list wouldn't update on cash amount change, needs some investigation)
- UI
- Moving some data-related logic from Presenter 

### Problems

API for some reason returns either json or html response randomly. Workaround used: not refreshing list on error response.