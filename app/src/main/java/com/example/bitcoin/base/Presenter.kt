package com.example.bitcoin.base

interface Presenter<T : BaseView> {

    fun attachView(view: T)

    fun detachView()
}