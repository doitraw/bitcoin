package com.example.bitcoin.base

abstract class BasePresenter<T : BaseView> : Presenter<T> {
    protected var view: T? = null

    override fun attachView(view: T) {
        this.view = view
    }

    override fun detachView() {
        this.view = null
    }
}