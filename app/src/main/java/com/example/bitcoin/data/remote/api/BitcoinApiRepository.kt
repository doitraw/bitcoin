package com.example.bitcoin.data.remote.api

import com.example.bitcoin.model.TickerData
import com.example.bitcoin.common.toPresentationModel
import com.example.bitcoin.data.remote.BitcoinRepository
import io.reactivex.Observable

class BitcoinApiRepository(private val bitcoinApi: BitcoinApi) :
    BitcoinRepository {
    override fun getBitcoinData(): Observable<List<TickerData>> =
        bitcoinApi.getBitcoinData().map { apiResponse ->
            apiResponse.ticker.markets.map { it.toPresentationModel() }
        }
}
