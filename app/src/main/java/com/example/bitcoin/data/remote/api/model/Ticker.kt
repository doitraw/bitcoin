package com.example.bitcoin.data.remote.api.model

data class Ticker(
    val base: String,
    val change: String,
    val price: String,
    val target: String,
    val volume: String,
    val markets: List<MarketData>
)