package com.example.bitcoin.data.remote.api.model

data class MarketData(
    val market: String,
    val price: String,
    val volume: Double
)