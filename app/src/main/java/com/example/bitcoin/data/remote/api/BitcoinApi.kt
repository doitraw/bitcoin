package com.example.bitcoin.data.remote.api

import com.example.bitcoin.data.remote.api.model.ApiResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Headers

interface BitcoinApi {

    @Headers("Content-Type: application/json")
    @GET("full/btc-usd")
    fun getBitcoinData(): Observable<ApiResponse>
}