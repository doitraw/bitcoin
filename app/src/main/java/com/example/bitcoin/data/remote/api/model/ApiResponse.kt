package com.example.bitcoin.data.remote.api.model

data class ApiResponse(
    val error: String,
    val success: Boolean,
    val ticker: Ticker,
    val timestamp: String
)