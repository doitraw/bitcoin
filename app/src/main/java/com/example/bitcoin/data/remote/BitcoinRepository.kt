package com.example.bitcoin.data.remote

import com.example.bitcoin.model.TickerData
import io.reactivex.Observable

interface BitcoinRepository {
    fun getBitcoinData(): Observable<List<TickerData>>
}