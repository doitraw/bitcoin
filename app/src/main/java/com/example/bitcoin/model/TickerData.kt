package com.example.bitcoin.model

data class TickerData(
    val market: String = "",
    val price: Double = 0.0,
    val volume: Double = 0.0,
    var indicator: Int = 0,
    var amount: Double = 0.0,
    var avgPrice: Double = price
)