package com.example.bitcoin.model

enum class SortingCriteria {
    MARKET,
    AVG_PRICE,
    VOLUME
}