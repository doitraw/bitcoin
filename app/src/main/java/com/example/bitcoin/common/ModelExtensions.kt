package com.example.bitcoin.common

import com.example.bitcoin.data.remote.api.model.MarketData
import com.example.bitcoin.model.SortingCriteria
import com.example.bitcoin.model.TickerData

fun MarketData.toPresentationModel() = TickerData(market, price.toDouble(), volume)

fun List<TickerData>.updateIndicatorAndAverage(oldList: List<TickerData>, time: Long): List<TickerData> =
    this.onEach { ticker ->
        oldList.firstOrNull { ticker.market == it.market }?.apply {
            ticker.indicator = this.price.compareTo(ticker.price)
            ticker.avgPrice = (this.avgPrice * (time - 1) + ticker.price) / time
        }
    }


fun List<TickerData>.sortBy(criteria: SortingCriteria): List<TickerData> =
    when (criteria) {
        SortingCriteria.MARKET -> this.sortedBy { it.market }
        SortingCriteria.AVG_PRICE -> this.sortedBy { it.avgPrice }
        SortingCriteria.VOLUME -> this.sortedBy { it.volume }
    }

fun List<TickerData>.updateAmount(cash: Double) = this.onEach {
    it.amount = cash.div(it.price)
}