package com.example.bitcoin.common

import io.reactivex.Scheduler

class SchedulerProvider(
    val mainThread: Scheduler,
    val bgThread: Scheduler
)