package com.example.bitcoin.main

import com.example.bitcoin.base.BaseView
import com.example.bitcoin.model.SortingCriteria
import com.example.bitcoin.model.TickerData

interface MainContract {
    interface View : BaseView {
        fun showData(list: List<TickerData>)
        fun showError(show: Boolean)
        fun showLoading(show: Boolean)
    }

    interface Presenter : com.example.bitcoin.base.Presenter<View> {
        fun onLoadData()
        fun onCashChanged(cash: String)
        fun onSortingCriteriaChanged(criteria: SortingCriteria)
    }


}