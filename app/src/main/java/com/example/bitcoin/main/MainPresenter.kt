package com.example.bitcoin.main

import com.example.bitcoin.base.BasePresenter
import com.example.bitcoin.common.SchedulerProvider
import com.example.bitcoin.common.sortBy
import com.example.bitcoin.common.updateAmount
import com.example.bitcoin.common.updateIndicatorAndAverage
import com.example.bitcoin.data.remote.BitcoinRepository
import com.example.bitcoin.model.SortingCriteria
import com.example.bitcoin.model.TickerData
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import java.util.concurrent.TimeUnit

class MainPresenter(
    private val bitcoinRepository: BitcoinRepository,
    private val schedulerProvider: SchedulerProvider
) : MainContract.Presenter, BasePresenter<MainContract.View>() {

    private var compositeDisposable = CompositeDisposable()
    private var list: List<TickerData> = listOf()
    private var cashAmount: Double = 0.0
    private var sortingCriteria = SortingCriteria.MARKET
    private var time = 0L

    override fun onLoadData() {
        compositeDisposable.add(
            Observable.interval(0, 10, TimeUnit.SECONDS)
                .flatMap {
                    bitcoinRepository.getBitcoinData()
                        .onErrorReturnItem(listOf())
                }.map {
                    it.takeIf { it.isNotEmpty() }
                        ?.updateIndicatorAndAverage(list, time++)
                        ?.updateAmount(cashAmount)
                        ?.sortBy(sortingCriteria)
                        ?: listOf()
                }
                .subscribeOn(schedulerProvider.bgThread)
                .observeOn(schedulerProvider.mainThread)
                .subscribe(
                    {
                        if (it.isNotEmpty()) {
                            list = it
                            view?.apply {
                                showLoading(false)
                                showData(list)
                            }
                        }
                    },
                    {
                        view?.showError(true)
                    }
                )
        )
    }

    override fun onCashChanged(cash: String) {
        cashAmount = cash.takeIf { it.isNotEmpty() }?.toDouble() ?: 0.0
        list = list.updateAmount(cash.toDouble())
        view?.showData(list)
    }

    override fun onSortingCriteriaChanged(criteria: SortingCriteria) {
        sortingCriteria = criteria
        list = list.sortBy(criteria)
        view?.showData(list)
    }

    override fun detachView() {
        compositeDisposable.dispose()
        super.detachView()
    }
}