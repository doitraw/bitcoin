package com.example.bitcoin.main

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.bitcoin.R
import com.example.bitcoin.model.SortingCriteria
import com.example.bitcoin.model.TickerData
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject

class MainActivity : AppCompatActivity(), MainContract.View {

    private val presenter: MainContract.Presenter by inject()
    private val bitcoinDataAdapter: BitcoinDataAdapter by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        amountET.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                presenter.onCashChanged(p0.toString())
            }
        })
        bitcoinDataRV.apply {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = bitcoinDataAdapter
        }
    }

    override fun onStart() {
        super.onStart()
        presenter.apply {
            attachView(this@MainActivity)
            onLoadData()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.my_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.market -> presenter.onSortingCriteriaChanged(SortingCriteria.MARKET)
            R.id.price -> presenter.onSortingCriteriaChanged(SortingCriteria.AVG_PRICE)
            R.id.volume -> presenter.onSortingCriteriaChanged(SortingCriteria.VOLUME)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showData(list: List<TickerData>) {
        bitcoinDataAdapter.updateList(list)
    }

    override fun showError(show: Boolean) {
        errorTv.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun showLoading(show: Boolean) {
        progressBar.visibility = if (show) View.VISIBLE else View.GONE
    }

    override fun onStop() {
        presenter.detachView()
        super.onStop()
    }
}
