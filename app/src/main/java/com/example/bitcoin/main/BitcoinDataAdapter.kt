package com.example.bitcoin.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.bitcoin.R
import com.example.bitcoin.model.TickerData
import kotlinx.android.synthetic.main.item_market.view.*


class BitcoinDataAdapter : RecyclerView.Adapter<BitcoinDataAdapter.ViewHolder>() {

    private val bitcoinData: MutableList<TickerData> = mutableListOf()

    fun updateList(newList: List<TickerData>) {
        bitcoinData.apply {
            clear()
            addAll(newList)
        }
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, index: Int) =
        ViewHolder(
            LayoutInflater.from(viewGroup.context).inflate(
                R.layout.item_market,
                viewGroup,
                false

            )
        )

    override fun getItemCount() = bitcoinData.size

    override fun onBindViewHolder(viewHolder: ViewHolder, index: Int) {
        viewHolder.bind(bitcoinData[index])
    }

    class ViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        internal fun bind(tickerData: TickerData) {
            view.apply {
                tickerData.apply {
                    marketTV.text = market
                    priceTV.text = context.getString(R.string.list_item_price, price.toString())
                    volumeTV.text = context.getString(R.string.list_item_volume, volume.toString())
                    amountTV.text = context.getString(R.string.list_item_amount, amount.toString())
                    avgPriceTV.text = context.getString(R.string.list_item_avg_price, avgPrice.toString())
                    indicatorIV.setImageDrawable(
                        when (indicator) {
                            -1 -> context.getDrawable(R.drawable.ic_arrow_upward_green)
                            1 -> context.getDrawable(R.drawable.ic_arrow_downward_red)
                            else -> null
                        }
                    )
                }
            }
        }
    }
}
