package com.example.bitcoin.di

import com.example.bitcoin.main.BitcoinDataAdapter
import com.example.bitcoin.main.MainContract
import com.example.bitcoin.main.MainPresenter
import com.example.bitcoin.common.SchedulerProvider
import com.example.bitcoin.data.remote.api.BitcoinApi
import com.example.bitcoin.data.remote.api.BitcoinApiRepository
import com.example.bitcoin.data.remote.BitcoinRepository
import com.google.gson.GsonBuilder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory


val appModule = module {
    single<BitcoinRepository> { BitcoinApiRepository(get()) }

    single<OkHttpClient> {
        OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .build()
    }

    single<Retrofit> {
        Retrofit.Builder()
            .baseUrl("https://api.cryptonator.com/api/")
            .client(get())
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().setLenient().create()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }


    single<BitcoinApi> { get<Retrofit>().create(BitcoinApi::class.java) }

    single { BitcoinDataAdapter() }

    single { SchedulerProvider(AndroidSchedulers.mainThread(), Schedulers.io()) }

    factory<MainContract.Presenter> {
        MainPresenter(
            get(),
            get()
        )
    }
}