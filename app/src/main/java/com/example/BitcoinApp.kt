package com.example

import android.app.Application
import com.example.bitcoin.di.appModule
import org.koin.android.ext.android.startKoin

class BitcoinApp : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(appModule))
    }
}